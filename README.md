# safe-and-smart-highway


* this project is about how to provide safety to the user on highway by providing information about nearby help center on highway
* automatic emergency broadcast message forward can be on/off from app setting.
* anyone from phonebook contact list can be added in friends list to contact in case of emergency on highway.
* google or facebook or email login options are available to the user others than email and phone number.
* All relatives added in a friends/relatives list get notified by push notification in case of any kind of emergency on the highway.
> Common chat panel like whatsapp group chat get initantiated when one open pushed notification in case of alert created by User Which remains active until alert creator not mark it solved.
